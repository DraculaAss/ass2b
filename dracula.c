// dracula.c
// Implementation of your "Fury of Dracula" Dracula AI

#include <stdlib.h>
#include <stdio.h>
#include "Game.h"
#include "DracView.h"

void decideDraculaMove(DracView gameState)
{
	/*
	THE STRAT
	Start in Madrid
	Run around in circles
	If hunters are near, go to Castle Dracula
	Run around in circles
	If hunters are near, get back to Madrid
	Repeat
	*/
	int numLocations;
	int road = TRUE;
	int sea = TRUE;
	int flee;
	int found = FALSE;
	int j;
	LocationID myTrail[TRAIL_SIZE];

	if (howHealthyIs(gameState, PLAYER_DRACULA) < 40
		|| whereIs(gameState, PLAYER_DRACULA) == whereIs(gameState, PLAYER_LORD_GODALMING)
		|| whereIs(gameState, PLAYER_DRACULA) == whereIs(gameState, PLAYER_DR_SEWARD )
		|| whereIs(gameState, PLAYER_DRACULA) == whereIs(gameState, PLAYER_VAN_HELSING)
		|| whereIs(gameState, PLAYER_DRACULA) == whereIs(gameState, PLAYER_MINA_HARKER)){
		if (whereIs(gameState, PLAYER_LORD_GODALMING) == CONSTANTA
			|| whereIs(gameState, PLAYER_LORD_GODALMING) == GALATZ
			|| whereIs(gameState, PLAYER_LORD_GODALMING) == CASTLE_DRACULA
			|| whereIs(gameState, PLAYER_LORD_GODALMING) == KLAUSENBURG
			|| whereIs(gameState, PLAYER_LORD_GODALMING) == SZEGED
			|| whereIs(gameState, PLAYER_LORD_GODALMING) == BELGRADE
			|| whereIs(gameState, PLAYER_LORD_GODALMING) == BUCHAREST

			|| whereIs(gameState, PLAYER_DR_SEWARD) == CONSTANTA
			|| whereIs(gameState, PLAYER_DR_SEWARD) == GALATZ
			|| whereIs(gameState, PLAYER_DR_SEWARD) == CASTLE_DRACULA
			|| whereIs(gameState, PLAYER_DR_SEWARD) == KLAUSENBURG
			|| whereIs(gameState, PLAYER_DR_SEWARD) == SZEGED
			|| whereIs(gameState, PLAYER_DR_SEWARD) == BELGRADE
			|| whereIs(gameState, PLAYER_DR_SEWARD) == BUCHAREST

			|| whereIs(gameState, PLAYER_VAN_HELSING) == CONSTANTA
			|| whereIs(gameState, PLAYER_VAN_HELSING) == GALATZ
			|| whereIs(gameState, PLAYER_VAN_HELSING) == CASTLE_DRACULA
			|| whereIs(gameState, PLAYER_VAN_HELSING) == KLAUSENBURG
			|| whereIs(gameState, PLAYER_VAN_HELSING) == SZEGED
			|| whereIs(gameState, PLAYER_VAN_HELSING) == BELGRADE
			|| whereIs(gameState, PLAYER_VAN_HELSING) == BUCHAREST

			|| whereIs(gameState, PLAYER_MINA_HARKER) == CONSTANTA
			|| whereIs(gameState, PLAYER_MINA_HARKER) == GALATZ
			|| whereIs(gameState, PLAYER_MINA_HARKER) == CASTLE_DRACULA
			|| whereIs(gameState, PLAYER_MINA_HARKER) == KLAUSENBURG
			|| whereIs(gameState, PLAYER_MINA_HARKER) == SZEGED
			|| whereIs(gameState, PLAYER_MINA_HARKER) == BELGRADE
			|| whereIs(gameState, PLAYER_MINA_HARKER) == BUCHAREST){
			flee = 0;
		}else{
			flee = 1;
		}
	}else{
		flee = 0;
	}

	if (giveMeTheRound(gameState) == 0){
		registerBestPlay("MA", "I'm free");
	}else{

		LocationID *connectedLocations = whereCanIgo(gameState, &numLocations, road, sea);
		LocationID selectedLocation = connectedLocations[0];
		char *locationAbbrev = idToAbbrev(selectedLocation);
		registerBestPlay(locationAbbrev, "Something went wrong");

		//check if possible move is in trail
        giveMeTheTrail(gameState, PLAYER_DRACULA, myTrail);

		if (idToType(whereIs(gameState, PLAYER_DRACULA)) == SEA){
				switch (whereIs(gameState, PLAYER_DRACULA)){
					case ATLANTIC_OCEAN: registerBestPlay("MS", "sea"); break;
					case MEDITERRANEAN_SEA:
											found = FALSE;
											for (j = 0; j < TRAIL_SIZE; j++){
	                							if (TYRRHENIAN_SEA == myTrail[j]){
								                    found = TRUE;
								                }
								            }
								            if (found == TRUE){
								            	registerBestPlay("AL", "sea");
								            }else{
								            	registerBestPlay("TS", "sea");
								            }; break;
					case BAY_OF_BISCAY: registerBestPlay("AO", "sea"); break;
					case TYRRHENIAN_SEA:
										found = FALSE;
										for (j = 0; j < TRAIL_SIZE; j++){
                							if (IONIAN_SEA == myTrail[j]){
							                    found = TRUE;
							                }
							            }
							            if (found == TRUE){
							            	registerBestPlay("MS", "sea");
							            }else{
							            	registerBestPlay("IO", "sea");
							            }; break;
					case IONIAN_SEA:
									found = FALSE;
									for (j = 0; j < TRAIL_SIZE; j++){
            							if (BLACK_SEA == myTrail[j]){
						                    found = TRUE;
						                }
						            }
						            if (found == TRUE){
						            	registerBestPlay("TS", "sea");
						            }else{
						            	registerBestPlay("BS", "sea");
						            }; break;
					case BLACK_SEA:
									found = FALSE;
									for (j = 0; j < TRAIL_SIZE; j++){
            							if (CONSTANTA == myTrail[j]){
						                    found = TRUE;
						                }
						            }
						            if (found == TRUE){
						            	registerBestPlay("IO", "sea");
						            }else{
						            	registerBestPlay("CN", "sea");
						            }; break;
				}
		/*
		if (idToType(whereIs(gameState, PLAYER_DRACULA)) == SEA){
			switch (whereIs(gameState, PLAYER_DRACULA)){
				case BLACK_SEA: registerBestPlay("IO", "sea"); break;
				case IONIAN_SEA: registerBestPlay("TS", "sea"); break;
				case TYRRHENIAN_SEA: registerBestPlay("MS", "sea"); break;
				case MEDITERRANEAN_SEA: registerBestPlay("AL", "sea"); break;
			}
		*/
		}else if (flee == 0){
			switch (whereIs(gameState, PLAYER_DRACULA)){
				//RUN IN CIRCLES
				case MADRID: registerBestPlay("LS", "loop"); break;
				case LISBON: registerBestPlay("CA", "loop"); break;
				case CADIZ: registerBestPlay("GR", "loop"); break;
				case GRANADA: registerBestPlay("AL", "loop"); break;
				case ALICANTE: registerBestPlay("SR", "loop"); break;
				case SARAGOSSA: registerBestPlay("SN", "loop"); break;
				case SANTANDER: registerBestPlay("MA", "loop"); break;

				//ESCAPE
				case CONSTANTA:
								found = FALSE;
								for (j = 0; j < TRAIL_SIZE; j++){
        							if (BLACK_SEA == myTrail[j]){
					                    found = TRUE;
					                }
					            }
					            if (found == TRUE){
					            	registerBestPlay("GA", "sea");
					            }else{
					            	registerBestPlay("BS", "sea");
					            }; break;
				case GALATZ: registerBestPlay("CD", "escape"); break;
				case CASTLE_DRACULA: registerBestPlay("KL", "escape"); break;
				case KLAUSENBURG: registerBestPlay("SZ", "escape"); break;
				case SZEGED: registerBestPlay("BE", "escape"); break;
				case BELGRADE: registerBestPlay("BC", "escape"); break;
				case BUCHAREST: registerBestPlay("CN", "escape"); break;
			}
		}else{
			switch (whereIs(gameState, PLAYER_DRACULA)){
				//ESCAPE

				case MADRID: registerBestPlay("LS", "run"); break;
				case LISBON: registerBestPlay("CA", "run"); break;
				case CADIZ: registerBestPlay("GR", "run"); break;
				case GRANADA: registerBestPlay("AL", "run"); break;
				case ALICANTE:
								found = FALSE;
								for (j = 0; j < TRAIL_SIZE; j++){
        							if (MEDITERRANEAN_SEA == myTrail[j]){
					                    found = TRUE;
					                }
					            }
					            if (found == TRUE){
					            	registerBestPlay("SR", "sea");
					            }else{
					            	registerBestPlay("MS", "sea");
					            }; break;
				case SARAGOSSA: registerBestPlay("SN", "run"); break;
				case SANTANDER: registerBestPlay("MA", "run"); break;

				/* gets dq because LS->AO->MS after returning from CD, MS still in path
				case MADRID: registerBestPlay("LS", "run"); break;
				case LISBON: registerBestPlay("AO", "run"); break;
				case CADIZ: registerBestPlay("AO", "run"); break;
				case GRANADA: registerBestPlay("AL", "run"); break;
				case ALICANTE:
								found = FALSE;
								for (j = 0; j < TRAIL_SIZE; j++){
        							if (MEDITERRANEAN_SEA == myTrail[j]){
					                    found = TRUE;
					                }
					            }
					            if (found == TRUE){
					            	registerBestPlay("MA", "sea");
					            }else{
					            	registerBestPlay("MS", "sea");
					            }; break;
				case SARAGOSSA: registerBestPlay("SN", "run"); break;
				case SANTANDER: registerBestPlay("BB", "run"); break;
				*/

				//HEAL
				case CONSTANTA: registerBestPlay("GA", "heal"); break;
				case GALATZ: registerBestPlay("CD", "heal"); break;
				case CASTLE_DRACULA: registerBestPlay("KL", "heal"); break;
				case KLAUSENBURG: registerBestPlay("SZ", "heal"); break;
				case SZEGED: registerBestPlay("BE", "heal"); break;
				case BELGRADE: registerBestPlay("BC", "heal"); break;
				case BUCHAREST: registerBestPlay("CN", "heal"); break;
			}
		}
	}
}

//1927 dryrun ass2b Makefile dracula.c hunter.c GameView.c GameView.h DracView.c DracView.h HunterView.c HunterView.h Map.h Map.c Places.h Places.c