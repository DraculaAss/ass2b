// hunter.c
// Implementation of your "Fury of Dracula" hunter AI

#include <stdlib.h>
#include <stdio.h>
#include "Game.h"
#include "HunterView.h"

void decideHunterMove(HunterView gameState)
{
    // TODO ...
    // Replace the line below by something better
    //registerBestPlay("GE","I'm on holiday in Geneva");
    int numLocations;
    int road = TRUE;
    int rail = TRUE;
    int sea = TRUE;
    Round r = giveMeTheRound(gameState);
    int i;
    int j;
    int found = FALSE;
    LocationID myTrail[TRAIL_SIZE];

    if (r == 0){
        switch (whoAmI(gameState)){
            case PLAYER_LORD_GODALMING: registerBestPlay("KL", "Lord Godalming awaits Dracula"); break;
            case PLAYER_DR_SEWARD: registerBestPlay("AT", "spawn"); break;
            case PLAYER_VAN_HELSING: registerBestPlay("CA", "spawn"); break;
            case PLAYER_MINA_HARKER: registerBestPlay("ED", "spawn"); break;
        }
    }else if (whoAmI(gameState) == PLAYER_LORD_GODALMING){
        if (whereIs(gameState, PLAYER_DRACULA) == CASTLE_DRACULA
            && (whereIs(gameState, PLAYER_LORD_GODALMING) == KLAUSENBURG
                || whereIs(gameState, PLAYER_LORD_GODALMING) == GALATZ)){
            registerBestPlay("CD", "I'm waiting");
        }
        else{
            switch (whereIs(gameState, PLAYER_LORD_GODALMING)){
                case KLAUSENBURG: registerBestPlay("GA", "def"); break;
                case GALATZ: registerBestPlay("KL", "def"); break;
                case CASTLE_DRACULA: registerBestPlay("KL", "def"); break;
                case ST_JOSEPH_AND_ST_MARYS: registerBestPlay("SZ", "def"); break;
                case SZEGED: registerBestPlay("KL", "def"); break;
            }
        }
    }else{
        LocationID *connectedLocations = whereCanIgo(gameState, &numLocations, road, rail, sea);
        LocationID selectedLocation = connectedLocations[0];
        char *locationAbbrev = idToAbbrev(selectedLocation);
        registerBestPlay(locationAbbrev, "hopefully i moved");

        //check if possible move is in trail
        giveMeTheTrail(gameState, whoAmI(gameState), myTrail);

        for (i = 0; i < numLocations; i++){
            found = FALSE;
            for (j = 0; j < TRAIL_SIZE; j++){
                if (connectedLocations[i] == myTrail[j]){
                    found = TRUE;
                }
            }
            if (found == FALSE){
                selectedLocation = connectedLocations[i];
                locationAbbrev = idToAbbrev(selectedLocation);
                registerBestPlay(locationAbbrev, "hopefully i moved");
            }
        }
    }
}
